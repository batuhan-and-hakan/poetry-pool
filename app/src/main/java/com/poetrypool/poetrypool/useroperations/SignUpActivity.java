package com.poetrypool.poetrypool.useroperations;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.poetrypool.poetrypool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    EditText editText, editText2, editText3, editText4, editText5;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        getSupportActionBar().setTitle("Sign Up");

        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        editText3 = findViewById(R.id.editText3);
        editText4 = findViewById(R.id.editText4);
        editText5 = findViewById(R.id.editText5);
        button = findViewById(R.id.button);

    }


    public void signup(View view) {

        if (editText.getText().toString().equals("") || editText2.getText().toString().equals("") || editText3.getText().toString().equals("") || editText4.getText().toString().equals("") || editText5.getText().toString().equals("")) {
            Toast.makeText(this, "All fields must not be empty!", Toast.LENGTH_LONG).show();
        }
        else if (!editText4.getText().toString().equals(editText5.getText().toString())) {
            Toast.makeText(this, "Passwords must be same!", Toast.LENGTH_LONG).show();
        }
        else if (!editText3.getText().toString().contains("@")) {
            Toast.makeText(this, "Wrong email format!", Toast.LENGTH_LONG).show();
        }
        else {

            button.setEnabled(false);

            String url = "http://batuhanbatu.net/poetrypoolwebservice/insert_user.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully added"))
                    {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
                        alertDialogBuilder.setTitle("Signed Up Successfully");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("Your profile was created successfully! Now, you can go to login page");

                        alertDialogBuilder.setPositiveButton("Go To Login", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        Toast.makeText(SignUpActivity.this, "This email is already registered to this app!", Toast.LENGTH_LONG).show();
                        button.setEnabled(true);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("fname", editText.getText().toString());
                    params.put("lname", editText2.getText().toString());
                    params.put("email", editText3.getText().toString());
                    params.put("password", editText4.getText().toString());
                    params.put("date", new Timestamp(System.currentTimeMillis()).toString());

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }

    }

}
