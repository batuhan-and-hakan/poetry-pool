package com.poetrypool.poetrypool.useroperations;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.poetrypool.poetrypool.R;

public class EnterTheCodeActivity extends AppCompatActivity {

    String code;
    String email;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_the_code);

        getSupportActionBar().setTitle("Enter The Code");
        editText = findViewById(R.id.editText);
        Intent getintent = getIntent();
        code = getintent.getStringExtra("code");
        email = getintent.getStringExtra("email");
    }

    public void continuee (View view) {

        if (editText.getText().toString().isEmpty())
        {
            Toast.makeText(this, "The field can not be empty!", Toast.LENGTH_LONG).show();
        }
        else
        {
            String entered = editText.getText().toString();

            if (!entered.equals(code))
                Toast.makeText(this, "Wrong code! Try again", Toast.LENGTH_LONG).show();

            else {
                Intent intent = new Intent(this, EnterNewPasswordActivity.class);
                intent.putExtra("email", email);
                startActivity(intent);
                finish();
            }
        }

    }
}
