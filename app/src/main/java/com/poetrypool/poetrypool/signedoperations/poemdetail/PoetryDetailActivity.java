package com.poetrypool.poetrypool.signedoperations.poemdetail;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.poetrypool.poetrypool.R;
import com.poetrypool.poetrypool.signedoperations.buy.PaymentMethodActivity;
import com.poetrypool.poetrypool.signedoperations.poetrylist.PoetryListActivity;
import com.poetrypool.poetrypool.signedoperations.profile.MyProfileActivity;
import com.poetrypool.poetrypool.signedoperations.profile.PoetProfileActivity;
import com.poetrypool.poetrypool.signedoperations.write.WritePoemActivity;
import com.poetrypool.poetrypool.useroperations.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PoetryDetailActivity extends AppCompatActivity {

    SharedPreferences session;
    int poetryid;
    int authorid;
    int initialpoint;

    TextView poetryname, description, content, authorname, tags;
    ImageView sharebutton, star1, star2, star3, star4, star5;
    LinearLayout linearlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poetry_detail);

        getSupportActionBar().setTitle("Poetry Details");
        session = getSharedPreferences("SESSION", MODE_PRIVATE);

        Intent getintent = getIntent();
        poetryid = Integer.valueOf(getintent.getStringExtra("poetryid"));

        poetryname = findViewById(R.id.poetryname);
        description = findViewById(R.id.description);
        content = findViewById(R.id.content);
        authorname = findViewById(R.id.authorname);
        tags = findViewById(R.id.tags);

        sharebutton = findViewById(R.id.sharebutton);
        star1 = findViewById(R.id.star1);
        star2 = findViewById(R.id.star2);
        star3 = findViewById(R.id.star3);
        star4 = findViewById(R.id.star4);
        star5 = findViewById(R.id.star5);

        linearlay = findViewById(R.id.linearlay);

        getPoemDetails();

        initializeStars();

    }

    public void initializeStars()
    {

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_buying_by_poetryid_buyerid.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("buying");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        initialpoint = Integer.valueOf(row.getString("point"));

                        if (initialpoint == 0)
                        {
                            star1.setImageResource(R.drawable.ic_star_empty);
                            star2.setImageResource(R.drawable.ic_star_empty);
                            star3.setImageResource(R.drawable.ic_star_empty);
                            star4.setImageResource(R.drawable.ic_star_empty);
                            star5.setImageResource(R.drawable.ic_star_empty);
                        }

                        else if (initialpoint == 1)
                        {
                            star1.setImageResource(R.drawable.ic_star_isset);
                            star2.setImageResource(R.drawable.ic_star_empty);
                            star3.setImageResource(R.drawable.ic_star_empty);
                            star4.setImageResource(R.drawable.ic_star_empty);
                            star5.setImageResource(R.drawable.ic_star_empty);
                        }

                        else if (initialpoint == 2)
                        {
                            star1.setImageResource(R.drawable.ic_star_isset);
                            star2.setImageResource(R.drawable.ic_star_isset);
                            star3.setImageResource(R.drawable.ic_star_empty);
                            star4.setImageResource(R.drawable.ic_star_empty);
                            star5.setImageResource(R.drawable.ic_star_empty);
                        }

                        else if (initialpoint == 3)
                        {
                            star1.setImageResource(R.drawable.ic_star_isset);
                            star2.setImageResource(R.drawable.ic_star_isset);
                            star3.setImageResource(R.drawable.ic_star_isset);
                            star4.setImageResource(R.drawable.ic_star_empty);
                            star5.setImageResource(R.drawable.ic_star_empty);
                        }

                        else if (initialpoint == 4)
                        {
                            star1.setImageResource(R.drawable.ic_star_isset);
                            star2.setImageResource(R.drawable.ic_star_isset);
                            star3.setImageResource(R.drawable.ic_star_isset);
                            star4.setImageResource(R.drawable.ic_star_isset);
                            star5.setImageResource(R.drawable.ic_star_empty);
                        }

                        else if (initialpoint == 5)
                        {
                            star1.setImageResource(R.drawable.ic_star_isset);
                            star2.setImageResource(R.drawable.ic_star_isset);
                            star3.setImageResource(R.drawable.ic_star_isset);
                            star4.setImageResource(R.drawable.ic_star_isset);
                            star5.setImageResource(R.drawable.ic_star_isset);
                        }

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("buyerid", String.valueOf(session.getInt("id", 0)));
                params.put("poetryid", String.valueOf(poetryid));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    public void goToPoetProfile(View view)
    {

        if (authorid == session.getInt("id", 0))
        {
            Intent intent = new Intent(PoetryDetailActivity.this, MyProfileActivity.class);
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(PoetryDetailActivity.this, PoetProfileActivity.class);
            intent.putExtra("poetid", authorid);
            startActivity(intent);
        }

    }

    public void getPoemDetails()
    {

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_poem_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("poetry");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        poetryname.setText(row.getString("name"));
                        description.setText(row.getString("description"));
                        content.setText(row.getString("content"));
                        authorname.setText(row.getString("authorname"));
                        authorid = Integer.valueOf(row.getString("authorid"));

                        if(authorid == session.getInt("id",0))
                            linearlay.setVisibility(View.GONE);

                        SpannableString underlinedTags = new SpannableString(row.getString("tags"));
                        underlinedTags.setSpan(new UnderlineSpan(), 0, underlinedTags.length(), 0);
                        tags.setText(underlinedTags);

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(poetryid));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }


    public void give1Point(View view)
    {

        String url = "http://batuhanbatu.net/poetrypoolwebservice/update_point.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains("Successfully updated"))
                {
                    star1.setImageResource(R.drawable.ic_star_isset);
                    star2.setImageResource(R.drawable.ic_star_empty);
                    star3.setImageResource(R.drawable.ic_star_empty);
                    star4.setImageResource(R.drawable.ic_star_empty);
                    star5.setImageResource(R.drawable.ic_star_empty);
                    Toast.makeText(PoetryDetailActivity.this, "You gave 1 point to this poem!", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(PoetryDetailActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("buyerid", String.valueOf(session.getInt("id", 0)));
                params.put("poetryid", String.valueOf(poetryid));
                params.put("point", "1");

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    public void give2Point(View view)
    {
        String url = "http://batuhanbatu.net/poetrypoolwebservice/update_point.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains("Successfully updated"))
                {
                    star1.setImageResource(R.drawable.ic_star_isset);
                    star2.setImageResource(R.drawable.ic_star_isset);
                    star3.setImageResource(R.drawable.ic_star_empty);
                    star4.setImageResource(R.drawable.ic_star_empty);
                    star5.setImageResource(R.drawable.ic_star_empty);
                    Toast.makeText(PoetryDetailActivity.this, "You gave 2 point to this poem!", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(PoetryDetailActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("buyerid", String.valueOf(session.getInt("id", 0)));
                params.put("poetryid", String.valueOf(poetryid));
                params.put("point", "2");

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void give3Point(View view)
    {
        String url = "http://batuhanbatu.net/poetrypoolwebservice/update_point.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains("Successfully updated"))
                {
                    star1.setImageResource(R.drawable.ic_star_isset);
                    star2.setImageResource(R.drawable.ic_star_isset);
                    star3.setImageResource(R.drawable.ic_star_isset);
                    star4.setImageResource(R.drawable.ic_star_empty);
                    star5.setImageResource(R.drawable.ic_star_empty);
                    Toast.makeText(PoetryDetailActivity.this, "You gave 3 point to this poem!", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(PoetryDetailActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("buyerid", String.valueOf(session.getInt("id", 0)));
                params.put("poetryid", String.valueOf(poetryid));
                params.put("point", "3");

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void give4Point(View view)
    {
        String url = "http://batuhanbatu.net/poetrypoolwebservice/update_point.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains("Successfully updated"))
                {
                    star1.setImageResource(R.drawable.ic_star_isset);
                    star2.setImageResource(R.drawable.ic_star_isset);
                    star3.setImageResource(R.drawable.ic_star_isset);
                    star4.setImageResource(R.drawable.ic_star_isset);
                    star5.setImageResource(R.drawable.ic_star_empty);
                    Toast.makeText(PoetryDetailActivity.this, "You gave 4 point to this poem!", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(PoetryDetailActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("buyerid", String.valueOf(session.getInt("id", 0)));
                params.put("poetryid", String.valueOf(poetryid));
                params.put("point", "4");

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void give5Point(View view)
    {
        String url = "http://batuhanbatu.net/poetrypoolwebservice/update_point.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains("Successfully updated"))
                {
                    star1.setImageResource(R.drawable.ic_star_isset);
                    star2.setImageResource(R.drawable.ic_star_isset);
                    star3.setImageResource(R.drawable.ic_star_isset);
                    star4.setImageResource(R.drawable.ic_star_isset);
                    star5.setImageResource(R.drawable.ic_star_isset);
                    Toast.makeText(PoetryDetailActivity.this, "You gave 5 point to this poem!", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(PoetryDetailActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("buyerid", String.valueOf(session.getInt("id", 0)));
                params.put("poetryid", String.valueOf(poetryid));
                params.put("point", "5");

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void shareThisPoem(View view)
    {

        Intent intent = new Intent(android.content.Intent.ACTION_SEND);

        String shareBody = content.getText().toString();
        String shareSubject = poetryname.getText().toString();

        intent.setType("text/plain");

        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSubject);
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);

        startActivity(Intent.createChooser(intent, "Share this poem to:"));

    }

    public void openPoetryList(View view)
    {
        Intent intentToPoetryList = new Intent(PoetryDetailActivity.this, PoetryListActivity.class);
        startActivity(intentToPoetryList);
    }

    public void openWritePoem(View view)
    {
        Intent intentToWritePoem = new Intent(PoetryDetailActivity.this, WritePoemActivity.class);
        startActivity(intentToWritePoem);
    }

    public void openMyProfile(View view)
    {
        Intent intentToPoetryList = new Intent(PoetryDetailActivity.this, MyProfileActivity.class);
        startActivity(intentToPoetryList);
    }

    public void signOut(View view)
    {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to sign out?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                session.edit().clear().apply();

                Intent intentToSignOut = new Intent(PoetryDetailActivity.this, MainActivity.class);
                intentToSignOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToSignOut);


            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();

    }

}