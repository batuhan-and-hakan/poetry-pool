package com.poetrypool.poetrypool.signedoperations.poetrylist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Layout;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.poetrypool.poetrypool.R;
import com.poetrypool.poetrypool.signedoperations.buy.PaymentMethodActivity;
import com.poetrypool.poetrypool.signedoperations.poemdetail.PoetryDetailActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PoetryListRecyclerAdapter extends RecyclerView.Adapter<PoetryListRecyclerAdapter.PostHolder> {

    private Context context;
    private String userid;
    private ArrayList<String> poemidList;
    private ArrayList<String> headerList;
    private ArrayList<String> descriptionList;
    private ArrayList<String> authorList;
    private ArrayList<String> priceList;
    private ArrayList<String> tagsList;


    public PoetryListRecyclerAdapter(Context context, String userid, ArrayList<String> poemidList, ArrayList<String> headerList, ArrayList<String> descriptionList, ArrayList<String> authorList, ArrayList<String> priceList, ArrayList<String> tagsList) {
        this.context = context;
        this.userid = userid;
        this.poemidList = poemidList;
        this.headerList = headerList;
        this.descriptionList = descriptionList;
        this.authorList = authorList;
        this.priceList = priceList;
        this.tagsList = tagsList;
    }

    @NonNull
    @Override
    public PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.poetry_recycler_row,parent,false);
        return new PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PostHolder holder, final int position) {

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "http://batuhanbatu.net/poetrypoolwebservice/is_this_poem_accessible.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray result = jsonObject.getJSONArray("buying");

                            for (int i = 0; i < result.length(); i++)
                            {
                                JSONObject row = result.getJSONObject(i);

                                Intent intent = new Intent(context, PoetryDetailActivity.class);
                                intent.putExtra("poetryid", poemidList.get(position));
                                context.startActivity(intent);

                            }

                        }

                        catch (JSONException e)
                        {
                            e.printStackTrace();
                            Intent intent = new Intent(context, PaymentMethodActivity.class);
                            intent.putExtra("poetryid", poemidList.get(position));
                            context.startActivity(intent);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("buyerid", userid);
                        params.put("poetryid", poemidList.get(position));

                        return params;
                    }
                };

                Volley.newRequestQueue(context).add(request);



            }
        });
        holder.headerText.setText(headerList.get(position));
        holder.descriptionText.setText(descriptionList.get(position));
        holder.authorText.setText(authorList.get(position));
        holder.priceText.setText(priceList.get(position));
        SpannableString contentt = new SpannableString(tagsList.get(position));
        contentt.setSpan(new UnderlineSpan(), 0, contentt.length(), 0);
        holder.tagsText.setText(contentt);
    }

    @Override
    public int getItemCount() {
        return headerList.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {

        ConstraintLayout container;
        TextView headerText;
        TextView descriptionText;
        TextView authorText;
        TextView priceText;
        TextView tagsText;

        public PostHolder(@NonNull final View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.poem_container);
            headerText = itemView.findViewById(R.id.headertextviewrecycler);
            descriptionText = itemView.findViewById(R.id.descriptiontextviewrecycler);
            authorText = itemView.findViewById(R.id.authortextviewrecycler);
            priceText = itemView.findViewById(R.id.pricetextviewrecycler);
            tagsText = itemView.findViewById(R.id.tagstextviewrecycler);

        }
    }
}
