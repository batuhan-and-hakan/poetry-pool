package com.poetrypool.poetrypool.signedoperations.buy;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.poetrypool.poetrypool.R;
import com.poetrypool.poetrypool.signedoperations.poemdetail.PoetryDetailActivity;
import com.poetrypool.poetrypool.signedoperations.poetrylist.PoetryListActivity;
import com.poetrypool.poetrypool.signedoperations.profile.MyProfileActivity;
import com.poetrypool.poetrypool.signedoperations.profile.PoetProfileActivity;
import com.poetrypool.poetrypool.signedoperations.write.WritePoemActivity;
import com.poetrypool.poetrypool.useroperations.MainActivity;
import com.poetrypool.poetrypool.useroperations.SignUpActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class PaymentMethodActivity extends AppCompatActivity {

    SharedPreferences session;
    int poetryid;
    int authorid;
    int price;
    int mybrightcoin;
    TextView headerTextView, descriptionTextView, authorTextView, priceTextView, tagsTextView;
    TextView textView, button;
    TextView textView2, button2, button3, button4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);



        getSupportActionBar().setTitle("Payment Method");
        session = getSharedPreferences("SESSION", MODE_PRIVATE);

        Intent getintent = getIntent();
        poetryid = Integer.valueOf(getintent.getStringExtra("poetryid"));

        headerTextView = findViewById(R.id.headertextviewrecycler);
        descriptionTextView = findViewById(R.id.descriptiontextviewrecycler);
        authorTextView = findViewById(R.id.authortextviewrecycler);
        priceTextView = findViewById(R.id.pricetextviewrecycler);
        tagsTextView = findViewById(R.id.tagstextviewrecycler);

        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button);

        textView2 = findViewById(R.id.textView2);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);

        getMyInformations();

    }

    public void getMyInformations()
    {

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_user_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("user");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        mybrightcoin = row.getInt("brightcoin");

                        initializeThePage();

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(PaymentMethodActivity.this, "Couldn't get your informations", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(PaymentMethodActivity.this).add(request);

    }

    public void initializeThePage()
    {

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_poem_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("poetry");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        headerTextView.setText(row.getString("name"));
                        descriptionTextView.setText(row.getString("description"));
                        authorTextView.setText(row.getString("authorname"));
                        priceTextView.setText(String.valueOf(row.getInt("price")));
                        tagsTextView.setText(row.getString("tags"));

                        price = row.getInt("price");
                        authorid = row.getInt("authorid");

                        if (price > mybrightcoin)
                        {
                            textView2.setVisibility(View.VISIBLE);
                            button2.setVisibility(View.VISIBLE);
                            button3.setVisibility(View.VISIBLE);
                            button4.setVisibility(View.VISIBLE);

                            textView2.setText("This poem costs " + price + " coins but you have " + mybrightcoin + " coins. You can buy a coin package in list:");

                        }

                        else
                        {
                            textView.setVisibility(View.VISIBLE);
                            button.setVisibility(View.VISIBLE);

                            textView.setText("This poem costs " + price + " coins and you have " + mybrightcoin + " coins. You can use your coins");

                        }

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(poetryid));

                return params;
            }
        };

        Volley.newRequestQueue(PaymentMethodActivity.this).add(request);

    }

    public void buyPoemWithCoin(View view)
    {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to buy this poem?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String url = "http://batuhanbatu.net/poetrypoolwebservice/insert_buying.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully added"))
                        {
                            Toast.makeText(PaymentMethodActivity.this, String.valueOf(price) + " coins were withdrawn from your account", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(PaymentMethodActivity.this, PoetryDetailActivity.class);
                            intent.putExtra("poetryid", String.valueOf(poetryid));
                            startActivity(intent);
                            finish();
                        }

                        else
                        {
                            Toast.makeText(PaymentMethodActivity.this, "Error!", Toast.LENGTH_LONG).show();
                            button.setEnabled(true);
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(PaymentMethodActivity.this, "Errorr!", Toast.LENGTH_LONG).show();

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("poetryid", String.valueOf(poetryid));
                        params.put("buyerid", String.valueOf(session.getInt("id", 0)));
                        params.put("price", String.valueOf(price));

                        return params;
                    }
                };

                Volley.newRequestQueue(PaymentMethodActivity.this).add(request);

            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();

    }

    public void buy10Coin(View view)
    {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Google Play");
        alertDialogBuilder.setMessage("1 ₺ will be taken from your account. Do you confirm?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String url = "http://batuhanbatu.net/poetrypoolwebservice/update_brightcoin.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully updated"))
                        {
                            Toast.makeText(PaymentMethodActivity.this, "10 Coins added to your account", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(PaymentMethodActivity.this, PaymentMethodActivity.class);
                            intent.putExtra("poetryid", String.valueOf(poetryid));
                            startActivity(intent);
                            finish();
                        }

                        else
                        {
                            Toast.makeText(PaymentMethodActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(PaymentMethodActivity.this, "Errorr!", Toast.LENGTH_LONG).show();

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("id", String.valueOf(session.getInt("id", 0)));
                        params.put("brightcoin", "10");

                        return params;
                    }
                };

                Volley.newRequestQueue(PaymentMethodActivity.this).add(request);

            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();

    }

    public void buy50Coin(View view)
    {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Google Play");
        alertDialogBuilder.setMessage("4 ₺ will be taken from your account. Do you confirm?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String url = "http://batuhanbatu.net/poetrypoolwebservice/update_brightcoin.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully updated"))
                        {
                            Toast.makeText(PaymentMethodActivity.this, "50 Coins added to your account", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(PaymentMethodActivity.this, PaymentMethodActivity.class);
                            intent.putExtra("poetryid", String.valueOf(poetryid));
                            startActivity(intent);
                            finish();
                        }

                        else
                        {
                            Toast.makeText(PaymentMethodActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(PaymentMethodActivity.this, "Errorr!", Toast.LENGTH_LONG).show();

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("id", String.valueOf(session.getInt("id", 0)));
                        params.put("brightcoin", "50");

                        return params;
                    }
                };

                Volley.newRequestQueue(PaymentMethodActivity.this).add(request);

            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();

    }

    public void buy100Coin(View view)
    {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Google Play");
        alertDialogBuilder.setMessage("7 ₺ will be taken from your account. Do you confirm?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String url = "http://batuhanbatu.net/poetrypoolwebservice/update_brightcoin.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Successfully updated"))
                        {
                            Toast.makeText(PaymentMethodActivity.this, "100 Coins added to your account", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(PaymentMethodActivity.this, PaymentMethodActivity.class);
                            intent.putExtra("poetryid", String.valueOf(poetryid));
                            startActivity(intent);
                            finish();
                        }

                        else
                        {
                            Toast.makeText(PaymentMethodActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(PaymentMethodActivity.this, "Errorr!", Toast.LENGTH_LONG).show();

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("id", String.valueOf(session.getInt("id", 0)));
                        params.put("brightcoin", "100");

                        return params;
                    }
                };

                Volley.newRequestQueue(PaymentMethodActivity.this).add(request);

            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();

    }

    public void openPoetryList(View view)
    {
        Intent intentToPoetryList = new Intent(PaymentMethodActivity.this, PoetryListActivity.class);
        startActivity(intentToPoetryList);
    }

    public void openWritePoem(View view)
    {
        Intent intentToWritePoem = new Intent(PaymentMethodActivity.this, WritePoemActivity.class);
        startActivity(intentToWritePoem);
    }

    public void openMyProfile(View view)
    {
        Intent intentToPoetryList = new Intent(PaymentMethodActivity.this, MyProfileActivity.class);
        startActivity(intentToPoetryList);
    }

    public void signOut(View view)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to sign out?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                session.edit().clear().apply();

                Intent intentToSignOut = new Intent(PaymentMethodActivity.this, MainActivity.class);
                intentToSignOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToSignOut);

            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
