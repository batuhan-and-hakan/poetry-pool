package com.poetrypool.poetrypool.signedoperations.profile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.poetrypool.poetrypool.R;
import com.poetrypool.poetrypool.signedoperations.buy.PaymentMethodActivity;
import com.poetrypool.poetrypool.signedoperations.poemdetail.PoetryDetailActivity;
import com.poetrypool.poetrypool.signedoperations.poetrylist.PoetryListActivity;
import com.poetrypool.poetrypool.signedoperations.poetrylist.PoetryListRecyclerAdapter;
import com.poetrypool.poetrypool.signedoperations.write.WritePoemActivity;
import com.poetrypool.poetrypool.useroperations.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyProfileActivity extends AppCompatActivity {

    TextView nameText, pointText, rustyCoinText, brightCoinText;

    SharedPreferences session;
    PoetryListRecyclerAdapter poetryListRecyclerAdapter;
    ArrayList<String> poemidFromFB;
    ArrayList<String> headerFromFB;
    ArrayList<String> descriptionFromFB;
    ArrayList<String> authorFromFB;
    ArrayList<String> priceFromFB;
    ArrayList<String> tagsFromFB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);

        nameText = findViewById(R.id.myfullname);
        pointText = findViewById(R.id.pointofme);
        rustyCoinText = findViewById(R.id.rustycoinofme);
        brightCoinText = findViewById(R.id.brightcoinofme);

        getTotalPointsOfMe();
        getMyCoins();

        nameText.setText(session.getString("fname", "") + " " + session.getString("lname", ""));

        poemidFromFB = new ArrayList<>();
        headerFromFB = new ArrayList<>();
        descriptionFromFB = new ArrayList<>();
        authorFromFB = new ArrayList<>();
        priceFromFB = new ArrayList<>();
        tagsFromFB = new ArrayList<>();

        getSupportActionBar().setTitle("My Profile");


        getDataFromFirestore();

        //RecyclerView

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        poetryListRecyclerAdapter = new PoetryListRecyclerAdapter(this, String.valueOf(session.getInt("id", 0)), poemidFromFB, headerFromFB,descriptionFromFB,authorFromFB,priceFromFB,tagsFromFB);
        recyclerView.setAdapter(poetryListRecyclerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.myprofile_options_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.buyedpoetrymenuitem) {
            Intent intentToPoetryList = new Intent(MyProfileActivity.this, BuyedPoetryActivity.class);
            startActivity(intentToPoetryList);
        }

        return super.onOptionsItemSelected(item);
    }

    public void getMyCoins()
    {

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_user_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("user");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        brightCoinText.setText(String.valueOf(row.getInt("brightcoin")));
                        rustyCoinText.setText(String.valueOf(row.getInt("rustycoin")));

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(MyProfileActivity.this, "Couldn't get your informations", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(MyProfileActivity.this).add(request);

    }

    public void getTotalPointsOfMe()
    {
        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_total_score.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("scores");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        pointText.setText(String.valueOf(row.getInt("point")));

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void getDataFromFirestore() {

        poemidFromFB.removeAll(poemidFromFB);
        headerFromFB.removeAll(headerFromFB);
        descriptionFromFB.removeAll(descriptionFromFB);
        authorFromFB.removeAll(authorFromFB);
        priceFromFB.removeAll(priceFromFB);
        tagsFromFB.removeAll(tagsFromFB);

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_poem_by_authorid.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("poetry");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        poemidFromFB.add(row.getString("id"));
                        headerFromFB.add(row.getString("name"));
                        descriptionFromFB.add(row.getString("description"));
                        authorFromFB.add(row.getString("authorname"));
                        priceFromFB.add(String.valueOf(row.getInt("price")));
                        tagsFromFB.add(row.getString("tags"));

                    }

                    poetryListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("authorid", String.valueOf(session.getInt("id", 0)));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    public void openPoetryList(View view)
    {
        Intent intentToPoetryList = new Intent(MyProfileActivity.this, PoetryListActivity.class);
        startActivity(intentToPoetryList);
    }

    public void openWritePoem(View view)
    {
        Intent intentToWritePoem = new Intent(MyProfileActivity.this, WritePoemActivity.class);
        startActivity(intentToWritePoem);
    }

    public void openMyProfile(View view)
    {

    }

    public void signOut(View view)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to sign out?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                session.edit().clear().apply();

                Intent intentToSignOut = new Intent(MyProfileActivity.this, MainActivity.class);
                intentToSignOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToSignOut);
            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
