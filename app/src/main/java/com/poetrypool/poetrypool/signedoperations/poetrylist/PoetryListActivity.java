package com.poetrypool.poetrypool.signedoperations.poetrylist;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.poetrypool.poetrypool.R;
import com.poetrypool.poetrypool.signedoperations.buy.PaymentMethodActivity;
import com.poetrypool.poetrypool.signedoperations.poemdetail.PoetryDetailActivity;
import com.poetrypool.poetrypool.signedoperations.profile.MyProfileActivity;
import com.poetrypool.poetrypool.signedoperations.profile.PoetProfileActivity;
import com.poetrypool.poetrypool.signedoperations.write.WritePoemActivity;
import com.poetrypool.poetrypool.useroperations.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class PoetryListActivity extends AppCompatActivity {

    //#496588 -> The color in logo

    SharedPreferences session;
    PoetryListRecyclerAdapter poetryListRecyclerAdapter;
    ArrayList<String> poemidFromFB;
    ArrayList<String> headerFromFB;
    ArrayList<String> descriptionFromFB;
    ArrayList<String> authorFromFB;
    ArrayList<String> priceFromFB;
    ArrayList<String> tagsFromFB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poetry_list);

        session = getSharedPreferences("SESSION", MODE_PRIVATE);
        getSupportActionBar().setTitle("Poetry List");
        //Toast.makeText(this, session.getString("fname", ""), Toast.LENGTH_LONG).show();

        poemidFromFB = new ArrayList<>();
        headerFromFB = new ArrayList<>();
        descriptionFromFB = new ArrayList<>();
        authorFromFB = new ArrayList<>();
        priceFromFB = new ArrayList<>();
        tagsFromFB = new ArrayList<>();

        getDataFromFirestore();

        //RecyclerView

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        poetryListRecyclerAdapter = new PoetryListRecyclerAdapter(this, String.valueOf(session.getInt("id", 0)), poemidFromFB, headerFromFB,descriptionFromFB,authorFromFB,priceFromFB,tagsFromFB);
        recyclerView.setAdapter(poetryListRecyclerAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.poetrylist_options_menu,menu);

        MenuItem searchItem = menu.findItem(R.id.app_bar_search);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {

                poemidFromFB.removeAll(poemidFromFB);
                headerFromFB.removeAll(headerFromFB);
                descriptionFromFB.removeAll(descriptionFromFB);
                authorFromFB.removeAll(authorFromFB);
                priceFromFB.removeAll(priceFromFB);
                tagsFromFB.removeAll(tagsFromFB);

                String url = "http://batuhanbatu.net/poetrypoolwebservice/search_in_poetry.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray result = jsonObject.getJSONArray("poetry");

                            for (int i = 0; i < result.length(); i++)
                            {
                                JSONObject row = result.getJSONObject(i);

                                poemidFromFB.add(row.getString("id"));
                                headerFromFB.add(row.getString("name"));
                                descriptionFromFB.add(row.getString("description"));
                                authorFromFB.add(row.getString("authorname"));
                                priceFromFB.add(String.valueOf(row.getInt("price")));
                                tagsFromFB.add(row.getString("tags"));

                            }

                            poetryListRecyclerAdapter.notifyDataSetChanged();
                        }

                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        poemidFromFB.removeAll(poemidFromFB);
                        headerFromFB.removeAll(headerFromFB);
                        descriptionFromFB.removeAll(descriptionFromFB);
                        authorFromFB.removeAll(authorFromFB);
                        priceFromFB.removeAll(priceFromFB);
                        tagsFromFB.removeAll(tagsFromFB);

                        poetryListRecyclerAdapter.notifyDataSetChanged();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("text", query);

                        return params;
                    }
                };

                Volley.newRequestQueue(PoetryListActivity.this).add(request);

                return false;

            }

            @Override
            public boolean onQueryTextChange(final String newText) {

                poemidFromFB.removeAll(poemidFromFB);
                headerFromFB.removeAll(headerFromFB);
                descriptionFromFB.removeAll(descriptionFromFB);
                authorFromFB.removeAll(authorFromFB);
                priceFromFB.removeAll(priceFromFB);
                tagsFromFB.removeAll(tagsFromFB);

                String url = "http://batuhanbatu.net/poetrypoolwebservice/search_in_poetry.php";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray result = jsonObject.getJSONArray("poetry");

                            for (int i = 0; i < result.length(); i++)
                            {
                                JSONObject row = result.getJSONObject(i);

                                poemidFromFB.add(row.getString("id"));
                                headerFromFB.add(row.getString("name"));
                                descriptionFromFB.add(row.getString("description"));
                                authorFromFB.add(row.getString("authorname"));
                                priceFromFB.add(String.valueOf(row.getInt("price")));
                                tagsFromFB.add(row.getString("tags"));

                            }

                            poetryListRecyclerAdapter.notifyDataSetChanged();
                        }

                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        poemidFromFB.removeAll(poemidFromFB);
                        headerFromFB.removeAll(headerFromFB);
                        descriptionFromFB.removeAll(descriptionFromFB);
                        authorFromFB.removeAll(authorFromFB);
                        priceFromFB.removeAll(priceFromFB);
                        tagsFromFB.removeAll(tagsFromFB);

                        poetryListRecyclerAdapter.notifyDataSetChanged();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<>();

                        params.put("text", newText);

                        return params;
                    }
                };

                Volley.newRequestQueue(PoetryListActivity.this).add(request);

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.sort_to_date) {
            getDataFromFirestore();
        }
        else if (item.getItemId() == R.id.sort_to_point) {
            getDataToTotalScore();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getDataToTotalScore() {

        poemidFromFB.removeAll(poemidFromFB);
        headerFromFB.removeAll(headerFromFB);
        descriptionFromFB.removeAll(descriptionFromFB);
        authorFromFB.removeAll(authorFromFB);
        priceFromFB.removeAll(priceFromFB);
        tagsFromFB.removeAll(tagsFromFB);

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_all_poetry_to_score.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("poetry");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        poemidFromFB.add(row.getString("id"));
                        headerFromFB.add(row.getString("name"));
                        descriptionFromFB.add(row.getString("description"));
                        authorFromFB.add(row.getString("authorname"));
                        priceFromFB.add(String.valueOf(row.getInt("price")));
                        tagsFromFB.add(row.getString("tags"));

                    }

                    poetryListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        Volley.newRequestQueue(this).add(request);

    }

    public void getDataFromFirestore() {

        poemidFromFB.removeAll(poemidFromFB);
        headerFromFB.removeAll(headerFromFB);
        descriptionFromFB.removeAll(descriptionFromFB);
        authorFromFB.removeAll(authorFromFB);
        priceFromFB.removeAll(priceFromFB);
        tagsFromFB.removeAll(tagsFromFB);

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_all_poetry_to_date.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("poetry");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        poemidFromFB.add(row.getString("id"));
                        headerFromFB.add(row.getString("name"));
                        descriptionFromFB.add(row.getString("description"));
                        authorFromFB.add(row.getString("authorname"));
                        priceFromFB.add(String.valueOf(row.getInt("price")));
                        tagsFromFB.add(row.getString("tags"));

                    }

                    poetryListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        Volley.newRequestQueue(this).add(request);

    }


    public void openPoetryList(View view)
    {

    }

    public void openWritePoem(View view)
    {
        Intent intentToWritePoem = new Intent(PoetryListActivity.this, WritePoemActivity.class);
        startActivity(intentToWritePoem);
    }

    public void openMyProfile(View view)
    {
        Intent intentToPoetryList = new Intent(PoetryListActivity.this, MyProfileActivity.class);
        startActivity(intentToPoetryList);
    }

    public void signOut(View view)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to sign out?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                session.edit().clear().apply();

                Intent intentToSignOut = new Intent(PoetryListActivity.this, MainActivity.class);
                intentToSignOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToSignOut);

            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

}
