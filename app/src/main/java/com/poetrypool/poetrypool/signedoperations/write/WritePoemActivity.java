package com.poetrypool.poetrypool.signedoperations.write;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.poetrypool.poetrypool.R;
import com.poetrypool.poetrypool.signedoperations.poemdetail.PoetryDetailActivity;
import com.poetrypool.poetrypool.signedoperations.poetrylist.PoetryListActivity;
import com.poetrypool.poetrypool.signedoperations.profile.BuyedPoetryActivity;
import com.poetrypool.poetrypool.signedoperations.profile.MyProfileActivity;
import com.poetrypool.poetrypool.useroperations.MainActivity;
import com.poetrypool.poetrypool.useroperations.SignUpActivity;

import java.util.HashMap;
import java.util.Map;

public class WritePoemActivity extends AppCompatActivity {

    SharedPreferences session;
    TextView nameOfThePoem, descriptionOfThePoem, contentOfThePoem, tagsOfThePoem, priceOfThePoem;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_poem);

        nameOfThePoem = findViewById(R.id.nameofthepoem);
        descriptionOfThePoem = findViewById(R.id.descriptionofthepoem);
        contentOfThePoem = findViewById(R.id.contentofthepoem);
        tagsOfThePoem = findViewById(R.id.tagsofthepoem);
        priceOfThePoem = findViewById(R.id.priceofthepoem);

        saveButton = findViewById(R.id.savepoembutton);

        getSupportActionBar().setTitle("Write Poem");
        session = getSharedPreferences("SESSION", MODE_PRIVATE);
    }

    public void addPoem(View view)
    {
        saveButton.setEnabled(false);

        if (nameOfThePoem.getText().toString().equals("") || descriptionOfThePoem.getText().toString().equals("") || contentOfThePoem.getText().toString().equals("") || tagsOfThePoem.getText().toString().equals("") || priceOfThePoem.getText().toString().equals(""))
        {
            Toast.makeText(this, "Please fill all blanks!", Toast.LENGTH_LONG).show();
            saveButton.setEnabled(true);
        }

        else
        {

            String url = "http://batuhanbatu.net/poetrypoolwebservice/insert_poem.php";

            StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("Successfully added"))
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WritePoemActivity.this);
                        alertDialogBuilder.setTitle("Poem is saved successfully!");
                        alertDialogBuilder.setCancelable(false);
                        alertDialogBuilder.setMessage("Your poem is successfully saved! Now, you can go to your profile");

                        alertDialogBuilder.setPositiveButton("Go To My Profile", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent intent = new Intent(WritePoemActivity.this, MyProfileActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    else
                    {
                        saveButton.setEnabled(true);
                        Toast.makeText(WritePoemActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Toast.makeText(WritePoemActivity.this, "Errorr!", Toast.LENGTH_SHORT).show();
                    saveButton.setEnabled(true);

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    params.put("authorid", String.valueOf(session.getInt("id", 0)));
                    params.put("name", nameOfThePoem.getText().toString());
                    params.put("description", descriptionOfThePoem.getText().toString());
                    params.put("content", contentOfThePoem.getText().toString());
                    params.put("price", priceOfThePoem.getText().toString());
                    params.put("tags", tagsOfThePoem.getText().toString());

                    return params;
                }
            };

            Volley.newRequestQueue(this).add(request);

        }

    }

    public void openPoetryList(View view)
    {
        Intent intentToPoetryList = new Intent(WritePoemActivity.this, PoetryListActivity.class);
        startActivity(intentToPoetryList);
    }

    public void openWritePoem(View view)
    {

    }

    public void openMyProfile(View view)
    {
        Intent intentToPoetryList = new Intent(WritePoemActivity.this, MyProfileActivity.class);
        startActivity(intentToPoetryList);
    }

    public void signOut(View view)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to sign out?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                session.edit().clear().apply();

                Intent intentToSignOut = new Intent(WritePoemActivity.this, MainActivity.class);
                intentToSignOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToSignOut);
            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

}