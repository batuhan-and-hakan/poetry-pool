package com.poetrypool.poetrypool.signedoperations.profile;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.poetrypool.poetrypool.R;
import com.poetrypool.poetrypool.signedoperations.buy.PaymentMethodActivity;
import com.poetrypool.poetrypool.signedoperations.poemdetail.PoetryDetailActivity;
import com.poetrypool.poetrypool.signedoperations.poetrylist.PoetryListActivity;
import com.poetrypool.poetrypool.signedoperations.poetrylist.PoetryListRecyclerAdapter;
import com.poetrypool.poetrypool.signedoperations.write.WritePoemActivity;
import com.poetrypool.poetrypool.useroperations.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PoetProfileActivity extends AppCompatActivity {

    TextView nameText, pointText;
    SharedPreferences session;
    int poetid;

    PoetryListRecyclerAdapter poetryListRecyclerAdapter;
    ArrayList<String> poemidFromFB;
    ArrayList<String> headerFromFB;
    ArrayList<String> descriptionFromFB;
    ArrayList<String> authorFromFB;
    ArrayList<String> priceFromFB;
    ArrayList<String> tagsFromFB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poet_profile);

        Intent getintent = getIntent();
        poetid = getintent.getIntExtra("poetid", 0);

        poemidFromFB = new ArrayList<>();
        headerFromFB = new ArrayList<>();
        descriptionFromFB = new ArrayList<>();
        authorFromFB = new ArrayList<>();
        priceFromFB = new ArrayList<>();
        tagsFromFB = new ArrayList<>();

        getSupportActionBar().setTitle("Poet Profile");
        session = getSharedPreferences("SESSION", MODE_PRIVATE);

        getPoetPoems();

        nameText = findViewById(R.id.poetfullname);
        pointText = findViewById(R.id.pointofthepoet);

        getPoetDetails();
        getTotalPointsOfThePoet();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        poetryListRecyclerAdapter = new PoetryListRecyclerAdapter(this, String.valueOf(session.getInt("id", 0)), poemidFromFB, headerFromFB,descriptionFromFB,authorFromFB,priceFromFB,tagsFromFB);
        recyclerView.setAdapter(poetryListRecyclerAdapter);
    }

    public void getPoetDetails()
    {

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_user_by_id.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("user");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        poemidFromFB.add(row.getString("id"));
                        nameText.setText(row.getString("fname") + " " + row.getString("lname"));

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(poetid));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    public void getPoetPoems()
    {

        poemidFromFB.removeAll(poemidFromFB);
        headerFromFB.removeAll(headerFromFB);
        descriptionFromFB.removeAll(descriptionFromFB);
        authorFromFB.removeAll(authorFromFB);
        priceFromFB.removeAll(priceFromFB);
        tagsFromFB.removeAll(tagsFromFB);

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_poem_by_authorid.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("poetry");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        poemidFromFB.add(row.getString("id"));
                        headerFromFB.add(row.getString("name"));
                        descriptionFromFB.add(row.getString("description"));
                        authorFromFB.add(row.getString("authorname"));
                        priceFromFB.add(String.valueOf(row.getInt("price")));
                        tagsFromFB.add(row.getString("tags"));

                    }

                    poetryListRecyclerAdapter.notifyDataSetChanged();
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("authorid", String.valueOf(poetid));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }

    public void getTotalPointsOfThePoet()
    {

        String url = "http://batuhanbatu.net/poetrypoolwebservice/get_total_score.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("scores");

                    for (int i = 0; i < result.length(); i++)
                    {
                        JSONObject row = result.getJSONObject(i);

                        pointText.setText(String.valueOf(row.getInt("point")));

                    }

                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("id", String.valueOf(poetid));

                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);

    }



    public void openPoetryList(View view)
    {
        Intent intentToPoetryList = new Intent(PoetProfileActivity.this, PoetryListActivity.class);
        startActivity(intentToPoetryList);
    }

    public void openWritePoem(View view)
    {
        Intent intentToWritePoem = new Intent(PoetProfileActivity.this, WritePoemActivity.class);
        startActivity(intentToWritePoem);
    }

    public void openMyProfile(View view)
    {
        Intent intentToPoetryList = new Intent(PoetProfileActivity.this, MyProfileActivity.class);
        startActivity(intentToPoetryList);
    }

    public void signOut(View view)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Are You Sure?");
        alertDialogBuilder.setMessage("Do you really want to sign out?");

        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                session.edit().clear().apply();

                Intent intentToSignOut = new Intent(PoetProfileActivity.this, MainActivity.class);
                intentToSignOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentToSignOut);


            }
        });

        alertDialogBuilder.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
